variable "aws_vpc" {
  description = "virtual private center"
  default     = "awsvpc"
}

variable "access_key" {
  default = "<PUT IN YOUR AWS ACCESS KEY>"
}
variable "secret_key" {
  default = "<PUT IN YOUR AWS SECRET KEY>"
}
variable "region" {
  default = "us-east-1"
}
variable "availabilityZone" {
  default = "us-east-1a"
}
variable "instanceTenancy" {
  default = "default"
}
variable "dnsSupport" {
  default = true
}
variable "dnsHostNames" {
  default = true
}
variable "vpcCIDRblock" {
  default = "172.17.0.0/16"
}

variable "destinationCIDRblock" {
  default = "0.0.0.0/0"
}
variable "ingressCIDRblock" {
  type    = list
  default = ["0.0.0.0/0"]
}
variable "egressCIDRblock" {
  type    = list
  default = ["0.0.0.0/0"]
}
variable "mapPublicIP" {
  default = true
}

variable "az_count" {
  description = "Number of AZs to cover in a given region"
  default     = "2"
}

variable "bastion_ami_image_id" {
  description = "Ubuntu Server 18.04 LTS (HVM), SSD Volume Type in us-east-1"
  default     = "ami-085925f297f89fce1"
}

variable "bastion_instance_type" {
  description = "EC2 instance type"
  default     = "t2.micro"
}

variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "sso@inrhythm.com"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}